import { AngularFirestore } from '@angular/fire/firestore';
import { Subject } from 'rxjs';
import { Data } from './data.model';

export class ThisThatService{
  dataChanged = new Subject<Data[]>();
  constructor(private db: AngularFirestore) {
    
  }

  fetchData() {
    this.db.collection('Info').valueChanges().subscribe((results: any[]) => {
      this.dataChanged.next(results);
    });
  }

  addData(data: Data) {
    this.db.collection('Info').add(data);
  }
}