import { Component, OnInit } from '@angular/core';
import { CountriesService } from './countries.model';
import { ThisThatService } from './this-that.service';
import { Data } from './data.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CountriesService]
})
export class AppComponent implements OnInit {
  title = 'this-that-project';
  countries = [];
  flag: string;
  genders = ['Male', 'Female'];
  genderString: string;
  gender: number;
  data: Data[];
  resultView: {
    totalData: number,
    totalThis: number,
    totalThat: number,
    thisPercent: number,
    thatPercent: number,
    malePercent: number,
    femalePercent: number
  };

  constructor(private countriesService: CountriesService, private thisThatService: ThisThatService) {

  }

  ngOnInit(): void {
    this.renewCountry();
    this.fetachData();
    this.thisThatService.dataChanged.subscribe((result: Data[]) => {
      this.data = result;
      this.populateData(this.data);
    });
  }

  renewCountry() {
    this.countries = this.countriesService.getCountries();
    const rnd = Math.abs(Math.floor(Math.random() * this.countries.length - 2));
    this.gender = Math.floor(Math.random() * 2);
    this.genderString = this.genders[this.gender];
    this.flag = this.countries[rnd].name;
  }

  fetachData() {
    this.thisThatService.fetchData();
  }

  populateData(data: Data[]) {
    const thisData =  data.filter(p => p.type === 'this');
    const thatData =  data.filter(p => p.type === 'that');
    const maleData =  data.filter(p => p.gender === 1);
    const femaleData =  data.filter(p => p.gender === 0);
    this.resultView = {
      totalData: data.length,
      totalThis: thisData.length,
      totalThat: thatData.length,
      thisPercent: Math.round((thisData.length / data.length * 100) * 100 / 100),
      thatPercent: Math.round((thatData.length / data.length * 100) * 100 / 100),
      malePercent: Math.round((maleData.length / data.length * 100) * 100 / 100),
      femalePercent: Math.round((femaleData.length / data.length * 100) * 100 / 100),
    };
  }

  onAddData(typeInput: string) {
    this.thisThatService.addData({country: this.flag, type: typeInput, gender: this.gender});
    this.renewCountry();
  }
}











