export class CountriesService {
  private countries = [
    {id: 0, name: 'ad', fullName: 'Andorra'},
    {id: 1, name: 'ae', fullName: ' Arab Emirates'},
    {id: 2, name: 'af', fullName: 'Afghanistan'},
    {id: 3, name: 'ag', fullName: 'Antigua'},
    {id: 4, name: 'ai', fullName: 'Anguilla'},
    {id: 5, name: 'al', fullName: 'Albania'},
    {id: 6, name: 'am', fullName: 'Armenia'},
    // {id: 7, name: 'an'},
    // {id: 8, name: 'ao'},
    // {id: 9, name: 'aq'},
    // {id: 10, name: 'ar'},
    // {id: 11, name: 'as'},
    // {id: 12, name: 'at'},
    // {id: 13, name: 'au'},
    // {id: 14, name: 'aw'},
    // {id: 15, name: 'ax'},
    // {id: 16, name: 'az'},
    // {id: 17, name: 'ba'},
    // {id: 18, name: 'bb'},
    // {id: 19, name: 'bd'},
    // {id: 20, name: 'be'},
    // {id: 21, name: 'bf'},
    // {id: 22, name: 'bg'},
    // {id: 23, name: 'bh'},
    // {id: 24, name: 'bs'},
    // {id: 25, name: 'bt'},
    // {id: 26, name: 'bv'},
    // {id: 27, name: 'bw'},
  ];

  getCountries() {
    return this.countries;
  }
}
