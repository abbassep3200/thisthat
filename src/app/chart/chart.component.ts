import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Data } from '../data.model';
import { CountriesService } from '../countries.model';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, OnChanges {

    @Input() data: Data[];


    // options for the chart
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    timeline = true;
    view: any[] = [600, 400];
    chartData = [];
    colorScheme = {
      domain: ['#ff61a1', '#ffc4da', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
    };
    showLabels = true;
  constructor(private countriesService: CountriesService) { 

  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data) {
      this.data = changes.data.currentValue;
      if (this.data) {
        this.createChart();
      }
    }
   }
  
  createChart() {
    this.chartData = [];
    const countries = [...new Set(this.data.map(p => p.country))];
    countries.forEach(element => {
      const males = this.data.filter(p => p.country === element && p.gender === 1).length;
      const females = this.data.filter(p => p.country === element && p.gender === 0).length;
      this.chartData.push({
        name: this.countriesService.getCountries().find(p => p.name === element).fullName,
        series: [
          {
            name: 'Male',
            value: males
          },
          {
            name: 'Female',
            value: females
          }
        ]
      });
    });
  }

}
