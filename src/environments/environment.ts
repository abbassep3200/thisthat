// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAUInOkuvI3BFD_lkax7bbPKfSbEliIPsM",
    authDomain: "thisthat-a3d49.firebaseapp.com",
    databaseURL: "https://thisthat-a3d49.firebaseio.com",
    projectId: "thisthat-a3d49",
    storageBucket: "thisthat-a3d49.appspot.com",
    messagingSenderId: "53405947590",
    appId: "1:53405947590:web:3161379b9556da9032e9c7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
